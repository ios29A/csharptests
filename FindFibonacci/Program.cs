﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FindFibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            for (; ; )
            {
                var n = AskForNumber("Введите индекс в последовательности Фибоначчи (1-91, {0}):", 91);
                var iterations = AskForNumber("Введите количество итераций ({0:#,0}):", 1000000000);
                var iterationsMethod = AskForNumber("Введите тип итераций (1 - Linq, 2 - Tasks, 3 - Tasks Pure, 4 - Threads Pure) ({0}):", 4);
                if (iterationsMethod < 1 || iterationsMethod > 4)
                {
                    Console.WriteLine("Неверный тип !");
                    continue;
                }
                var method = 0;
                if (iterationsMethod != 3 && iterationsMethod != 4)
                {
                    method = AskForNumber("Введите тип расчета (1 - Linq, Олег Скрипняк, 2 - Александр Болдер) ({0}):", 2);
                    if (method != 1 && method != 2)
                    {
                        Console.WriteLine("Неверный метод !");
                        continue;
                    }
                }
                var threadsNumber = AskForNumber($"Ввведите кол-во потоков ({Environment.ProcessorCount}):", Environment.ProcessorCount);
                Console.WriteLine($"Расчет числа Фибоначчи {n}, итераций {iterations:#,0}, тип итерации {iterationsMethod}, метод {method}, потоков: {threadsNumber}, итераций на поток: {(int)Math.Ceiling((double)iterations / threadsNumber):#,0}");
                Func<int, int, int, int, Int64> findFibonacciFunc0;
                switch (iterationsMethod)
                {
                    case 1:
                        findFibonacciFunc0 = FindUsingLinq;
                        break;
                    case 2:
                        findFibonacciFunc0 = FindUsingTasks;
                        break;
                    case 3:
                        findFibonacciFunc0 = FindUsingTasksPure;
                        break;
                    default:
                        findFibonacciFunc0 = FindUsingThreadsPure;
                        break;
                }
                var watcher = System.Diagnostics.Stopwatch.StartNew();
                var fibonacci = findFibonacciFunc0(n, iterations, threadsNumber, method);
                Console.WriteLine($"Чиcло равно: {fibonacci}, полное время выполнения: {watcher.ElapsedMilliseconds}ms");
            }
        }

        static int AskForNumber(string text, int defaultValue)
        {
            Console.WriteLine(text, defaultValue);
            return int.TryParse(
                   Console.ReadLine()
                  .Replace(
                       System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator,
                       ""),
                  out var rv) ? rv : defaultValue;
        }

        static Int64 FindUsingLinq(int n, int iterations, int threadsNumber, int method)
        {
            Func<int, Int64> findFibonacciFunc1 = (method == 1) ? (Func<int, Int64>)FindFibonacci : FindFibonacciByAlex;
            return Enumerable.Range(0, iterations).AsParallel().WithDegreeOfParallelism(threadsNumber).Select(index => findFibonacciFunc1(n)).Last();
        }

        static Int64 FindUsingTasks(int n, int iterations, int threadsNumber, int method)
        {
            var iterationsPerThread = (int)Math.Ceiling((double)iterations / threadsNumber);
            Func<int, Int64> findFibonacciFunc1 = (method == 1) ? (Func<int, Int64>)FindFibonacci : FindFibonacciByAlex;
            Int64 rv = 0;
            var tasks = Enumerable.Range(0, threadsNumber).Select(i => Task.Factory.StartNew(() =>
             {
                 for (var j = 0; j < iterationsPerThread; j++)
                 {
                     rv = findFibonacciFunc1(n);
                 }
                 return rv;
             }, TaskCreationOptions.PreferFairness)).ToArray();
            Task.WaitAll(tasks);
            return tasks.First().Result;
        }

        static Int64 FindUsingTasksPure(int n, int iterations, int threadsNumber, int method)
        {
            var iterationsPerThread = (int)Math.Ceiling((double)iterations / threadsNumber);
            Int64 rv = 0;
            var tasks = Enumerable.Range(0, threadsNumber).Select(thread => Task.Factory.StartNew(() =>
            {
                var watcher = System.Diagnostics.Stopwatch.StartNew();
                Console.WriteLine($"Старт потока {Thread.CurrentThread.ManagedThreadId}, просчет {iterationsPerThread} итерации");
                Int64 res = 0;
                for (var j = 0; j < iterationsPerThread; j++)
                {
                    Int64 p1 = 1, p2 = 0;
                    for (var i = 0; i < n; i++)
                    {
                        res = p1 + p2;
                        p2 = p1;
                        p1 = res;
                    }
                }
                rv = res;
                Console.WriteLine($"Поток {Thread.CurrentThread.ManagedThreadId} исполнялся: {watcher.ElapsedMilliseconds}ms");
            }, TaskCreationOptions.PreferFairness)).ToArray();
            Task.WaitAll(tasks);
            return rv;
        }

        static Int64 FindUsingThreadsPure(int n, int iterations, int threadsNumber, int method)
        {
            var iterationsPerThread = (int)Math.Ceiling((double)iterations / threadsNumber);
            Int64 rv = 0;
            Enumerable.Range(0, threadsNumber).Select(id =>
            {
                var t = new Thread(() =>
                {
                    var watcher = System.Diagnostics.Stopwatch.StartNew();
                    Console.WriteLine($"Старт потока {Thread.CurrentThread.ManagedThreadId}, просчет {iterationsPerThread} итерации");
                    Int64 res = 0;
                    for (var j = 0; j < iterationsPerThread; j++)
                    {
                        Int64 p1 = 1, p2 = 0;
                        for (var i = 0; i < n; i++)
                        {
                            res = p1 + p2;
                            p2 = p1;
                            p1 = res;
                        }
                    }
                    rv = res;
                    Console.WriteLine($"Поток {Thread.CurrentThread.ManagedThreadId} исполнялся: {watcher.ElapsedMilliseconds}ms");
                });
                t.Start();
                return t;
            }).ToList().ForEach(t => t.Join());
            return rv;
        }

        static Int64 FindFibonacci(int n)
        {
            Int64 p1 = 1, p2 = 0, res = 0;
            return Enumerable.Range(0, n)
                .Select(index => { res = p1 + p2; p2 = p1; p1 = res; return res; })
                .Last(); // < Last() производит вычисление
        }

        static Int64 FindFibonacciByAlex(int n)
        {
            Int64 p1 = 1, p2 = 0, res = 0;
            for (var i = 0; i < n; i++)
            {
                res = p1 + p2;
                p2 = p1;
                p1 = res;
            }
            return p1;
        }
    }
}
