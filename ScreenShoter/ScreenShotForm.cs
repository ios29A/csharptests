﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ScreenShoter
{
    class ScreenShotForm : Form
    {
        protected Bitmap Bitmap;
        public ScreenShotForm(IntPtr catchWnd)
        {
            Text = "ScreenShot";
            Icon = Resource1.Camera;
            GetWindowRect(new HandleRef(this, catchWnd), out var rect);
            Bitmap = new Bitmap(rect.Right - rect.Left, rect.Bottom - rect.Top);
            using (var graphics = Graphics.FromImage(Bitmap))
            {
                graphics.CopyFromScreen(rect.Left, rect.Top, 0, 0, Bitmap.Size);
            }
            new System.Media.SoundPlayer(Resource1.CameraSound).Play();

            var currentColor = Color.Red;
            Point from = Point.Empty;

            MouseDown += (o, a) =>
                {
                    if (a.Button == MouseButtons.Left)
                    {
                        from = a.Location;
                        Capture = true;
                    }
                };

            MouseMove += (o, a) =>
                {
                    if (from != Point.Empty)
                    {
                        using (var graphics = Graphics.FromImage(Bitmap))
                        using (var pen = new Pen(currentColor))
                        {
                            graphics.DrawLine(pen, from, a.Location);
                        }
                        from = a.Location;
                        Invalidate();
                    }
                };

            MouseUp += (o, a) =>
                {
                    if (a.Button == MouseButtons.Left)
                    {
                        from = Point.Empty;
                        Capture = false;
                    }
                };

            MouseClick += (o, a) =>
                {
                    if (a.Button == MouseButtons.Right)
                    {
                        var colorDlg = new ColorDialog { Color = currentColor };
                        if (colorDlg.ShowDialog() == DialogResult.OK)
                        {
                            currentColor = colorDlg.Color;
                        }
                    }
                };

            var ext2ImageFormatMap = new Dictionary<string, ImageFormat>
                {
                    {".jpg", ImageFormat.Jpeg},
                    {".png", ImageFormat.Png},
                    {".gif", ImageFormat.Gif},
                    {".bmp", ImageFormat.Bmp},
                    {".tiff", ImageFormat.Tiff},
                };

            MouseDoubleClick += (o, a) =>
                {
                    if (a.Button == MouseButtons.Left)
                    {
                        var saveDlg = new SaveFileDialog
                        {
                            FileName = Text,
                            Filter = Resource1.BitmapFilter,
                            FilterIndex = 2
                        };
                        if (saveDlg.ShowDialog() == DialogResult.OK)
                        {
                            Bitmap.Save(saveDlg.FileName, ext2ImageFormatMap.TryGetValue(Path.GetExtension(saveDlg.FileName).ToLowerInvariant(), out var imgF) ? imgF : ImageFormat.Bmp);
                        }
                    }
                };

            FormClosed += (o, a) =>
                {
                    Bitmap.Dispose();
                };
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            e.Graphics.DrawImage(Bitmap, 0, 0);
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowRect(HandleRef hWnd, out RECT lpRect);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }
    }
}
