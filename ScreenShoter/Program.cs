﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Gma.System.MouseKeyHook;
using PeanutButter.TrayIcon;

namespace ScreenShoter
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var mutex = new Mutex(true, "ios29AScreenShooter", out var newMutexCreated))
            using (var trayIcon = new TrayIcon(Resource1.Camera))
            using (var globalHook = Hook.GlobalEvents())
            {
                if (!newMutexCreated)
                    return;
                trayIcon.AddMenuItem("Exit", () => Application.Exit());
                globalHook.KeyUp += (o, a) =>
                  {
                      if (a.Control && a.Shift && a.KeyCode == Keys.PrintScreen)
                      {
                          var f = new ScreenShotForm(GetForegroundWindow());
                          f.Show();
                          f.BringToFront();
                          SetForegroundWindow(f.Handle);
                          f.Activate();
                      }
                  };
                trayIcon.Show();
                Application.Run();
                trayIcon.Hide();
            }
        }

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();
    }
}
