﻿using System;
using System.Linq;

namespace FindSum
{
    class Program
    {

        static void Main(string[] args)
        {
            //-29,5,7,8,37,45,-33,55,67,-4, 17, 19, 95,55, 66, 77, 88, 99
            //126
            for (;;)
            {
                Console.WriteLine("Введите числа через запятую, или пустую строку для прекращения:");
                var numbers = Console.ReadLine().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => Int64.Parse(s.Trim())).ToArray();
                if (numbers.Length == 0)
                {
                    break;
                }
                Console.WriteLine("Введите сумму:");
                var dstNumber = Int64.Parse(Console.ReadLine().Trim());
                Console.WriteLine("Сколько раз повторить поиск ? по умолчанию (1):");
                if (!int.TryParse(Console.ReadLine().Trim(), out var tries))
                {
                    tries = 1;
                }
                var watcher = System.Diagnostics.Stopwatch.StartNew();
                var sequence = Enumerable.Range(0, tries).Select(i => FindSum(numbers, dstNumber)).Last();
                var elapsed = watcher.ElapsedMilliseconds;
                if (sequence != null)
                {
                    Console.WriteLine($"Найдена последовательность: {string.Join("", sequence.Select((n, index) => n > 0 ? $"+{n}" : $"{(index == 0 ? " " : "")}{n}")).Substring(1)}={dstNumber}");
                    Console.WriteLine($"Elapsed: {elapsed}ms");
                }
                else
                {
                    Console.WriteLine($"Последовательность не найдена !\r\nElapsed: {elapsed}ms");
                }
            }
        }
        static Int64[] FindSum(Int64[] numbers, Int64 dstNumber)
        {
            var totalCases = 1 << numbers.Length;
            for (var i = 1; i < totalCases; i++)
            {
                var sequence = numbers.Where((n, index) => (i & (1 << index)) > 0);
                if (sequence.Sum() == dstNumber)
                {
                    return sequence.ToArray();
                }
            }
            return null;
        }
    }
}
